﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tar : MonoBehaviour
{
	[HideInInspector] public float movementReduction = 0.4f;
	Rigidbody rigid;
	Collider coll;

	void Awake()
	{
		gameObject.layer = 9; //Flying tar layer
		rigid = GetComponent<Rigidbody>();
		coll = GetComponent<Collider>();
	}

	void OnCollisionEnter(Collision col)
	{
		if(col.gameObject.tag == "Ground")
		{
			gameObject.layer = 0; //Default
			rigid.isKinematic = true;
			coll.isTrigger = true;
		}
	}

	void OnTriggerStay(Collider col)
	{
		if(col.gameObject.tag == "Enemy")
		{
			EnemyBehavior colEnemy = col.gameObject.GetComponent<EnemyBehavior>();
			if(colEnemy != null)
			{
				colEnemy.rigid.velocity = (colEnemy.rigid.velocity * movementReduction);
			}
		}
	}
}
