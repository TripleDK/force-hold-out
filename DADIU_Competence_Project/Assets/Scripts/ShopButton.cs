﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopButton : MonoBehaviour
{
	public Shop shop;
	public Text buttonText;
	public ShopElement shopElement;
	[HideInInspector]public Button button;

	// Use this for initialization
	void Start()
	{
		button = GetComponent<Button>();
		button.onClick.AddListener(BuyItem);
	}

	void BuyItem()
	{
		Shop.UpgradeableItem upgrade = shop.shopUpgrades.Where(o => o.name == transform.parent.name).SingleOrDefault();
		Shop.PurchaseableItem item = shop.shopConsumables.Where(o => o.name == transform.parent.name).SingleOrDefault();
		if(upgrade != null)
		{
			if(upgrade.name == "TestUpgrade")
			{
				GameStateManager.money = 99999999;
			}
			shop.Buy(upgrade, this);
		}	

		if(item != null)
		{
			shop.Buy(item, this);
		}
	}

}
