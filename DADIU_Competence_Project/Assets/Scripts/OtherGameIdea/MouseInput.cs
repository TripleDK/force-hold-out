﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseInput : MonoBehaviour
{

	public GameObject[] selectedObjects;
	[SerializeField] RectTransform selectionBox;

	Vector3 startPos, endPos;
	Vector2 endScreenPos;

	// Use this for initialization
	void Start()
	{
		selectionBox = Instantiate(selectionBox, transform.position, Quaternion.identity).GetComponent<RectTransform>();
		selectionBox.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update()
	{
		if(Input.GetMouseButtonDown(0))
		{
			selectionBox.gameObject.SetActive(true);
			startPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			selectionBox.transform.position = startPos;
		
		
		}
		if(Input.GetMouseButton(0))
		{
			
		}
		if(Input.GetMouseButtonUp(0))
		{
			
		}
	}
}
