﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverlapTest : MonoBehaviour
{

	ContactFilter2D filter = new ContactFilter2D();
	public 	Collider2D[] colliders = new Collider2D[10];
	BoxCollider2D col;
	// Use this for initialization
	void Start()
	{
		col = gameObject.GetComponent<BoxCollider2D>();
		filter.NoFilter();
	}
	
	// Update is called once per frame
	void Update()
	{

		Debug.Log(Physics2D.OverlapBox(transform.position, col.size, 0));
	}
}
