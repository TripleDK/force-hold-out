﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OilBarrel : Explodable
{

	[SerializeField] float tarDuration = 5f;
	[SerializeField] float movementReduction = 0.8f;
	[SerializeField] Tar tar;

	public override void BlowUp()
	{
		Tar tempTar = Instantiate(tar, transform.position, tar.transform.rotation);
		tempTar.movementReduction = movementReduction;
		Destroy(tempTar.gameObject, tarDuration);
		Destroy(gameObject);
	}
}
