﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
	[System.Serializable]
	public class UpgradeableItem
	{
		public string name;
		public int upgradeLevel;
		public int[] prices;
	}

	[System.Serializable]
	public class PurchaseableItem
	{
		public string name;
		public int price;
	}

	public List<UpgradeableItem> shopUpgrades = new List<UpgradeableItem>();
	public List<PurchaseableItem> shopConsumables = new List<PurchaseableItem>();
	public Text castleHealth;
	[SerializeField] ShopElement shopElement;
	[SerializeField] Castle castle;
	[SerializeField] Text money;
	[SerializeField] Text nextLevel;

	Button saveButton;
	GameObject bombDepositField;
	Button bombDepositBuy, bombDepositUpgrade;

	void OnEnable()
	{
		money.text = "Money: " + GameStateManager.money;
		castleHealth.text = "Castle Health: \n" + castle.health + "/" + castle.maxHealth;
		nextLevel.text = "Next level: " + (EnemySpawner.currentLevel + 1).ToString("D");
	}

	// Use this for initialization
	void Start()
	{
		saveButton = transform.Find("SaveGame").GetComponent<Button>();

		for(int i = 0; i < shopConsumables.Count; i++)
		{
			ShopElement tempElement = Instantiate(shopElement, shopElement.transform.position, shopElement.transform.rotation);
			tempElement.transform.SetParent(shopElement.transform.parent, true);
			tempElement.transform.localScale = shopElement.transform.localScale;
			tempElement.gameObject.name = shopConsumables[i].name;
			tempElement.contentRectTransform.offsetMax = new Vector2(tempElement.contentRectTransform.offsetMax.x + 300, tempElement.contentRectTransform.offsetMax.y);
			tempElement.name.text = shopConsumables[i].name;
			tempElement.upgradeLevel.text = "";
			tempElement.price.text = shopConsumables[i].price.ToString("D");
			ShopButton tempButton = tempElement.shopButton;
			tempButton.buttonText.text = "Buy";
		}

		for(int i = 0; i < shopUpgrades.Count; i++)
		{
			ShopElement tempElement = Instantiate(shopElement, shopElement.transform.position, shopElement.transform.rotation);
			tempElement.transform.SetParent(shopElement.transform.parent, true);
			tempElement.transform.localScale = shopElement.transform.localScale;
			tempElement.gameObject.name = shopUpgrades[i].name;
			tempElement.contentRectTransform.offsetMax = new Vector2(tempElement.contentRectTransform.offsetMax.x + 300, tempElement.contentRectTransform.offsetMax.y);
			tempElement.name.text = shopUpgrades[i].name;
			tempElement.upgradeLevel.text = shopUpgrades[i].upgradeLevel.ToString("D");
			tempElement.price.text = shopUpgrades[i].prices[shopUpgrades[i].upgradeLevel].ToString("D");
			ShopButton tempButton = tempElement.shopButton;
			if(shopUpgrades[i].upgradeLevel == 0)
			{
				tempButton.buttonText.text = "Buy";
			} else
			{
				tempButton.buttonText.text = "Upgrade";
			}
			if(shopUpgrades[i].upgradeLevel == shopUpgrades[i].prices.Length - 1)
			{
				tempButton.button.interactable = false;
			}
		}
	

		shopElement.gameObject.SetActive(false);
	}



	public void Buy(UpgradeableItem item, ShopButton shopButton)
	{
		if(GameStateManager.money >= item.prices[item.upgradeLevel])
		{
			GameStateManager.money -= item.prices[item.upgradeLevel];
			item.upgradeLevel++;
			if(item.upgradeLevel == 1)
			{
				shopButton.buttonText.text = "Upgrade";
			}
			if(item.upgradeLevel == item.prices.Length - 1)
			{
				shopButton.button.interactable = false;
			}
			shopButton.shopElement.upgradeLevel.text = item.upgradeLevel.ToString("D");
			shopButton.shopElement.price.text = item.prices[item.upgradeLevel].ToString("D");
			money.text = "Money: " + GameStateManager.money;
			saveButton.interactable = true;
			castle.RefreshCastleUpgrade(item.name);
			Debug.Log("Just upgraded " + item.name + ", current level is: " + item.upgradeLevel);
		}
	
	}

	public void Buy(PurchaseableItem item, ShopButton shopButton)
	{
		if(GameStateManager.money >= item.price)
		{
			GameStateManager.money -= item.price;
			castle.ConsumeShopItem(item.name);
			money.text = "Money: " + GameStateManager.money;
			saveButton.interactable = true;
			Debug.Log("Just bought " + item.name);
		}
	}
}
