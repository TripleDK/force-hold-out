﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(EnemySpawner))]
public class EnemySpawnerEditor : Editor
{
	EnemySpawner enemySpawner;
	int levelIndex = 0;
	string[] levelNames = null;
	static int numOfEnemies = 1;
	static float timeBetween = 1;
	static EnemyBehavior enemyToSpawn;

	void OnEnable()
	{
		enemySpawner = (EnemySpawner)target;
		UpdateLevelNames();
		AlignArraySize();
	}

	public override void OnInspectorGUI()
	{
		enemySpawner = (EnemySpawner)target;
		if(levelNames == null)
		{
			UpdateLevelNames();
		}
		serializedObject.Update();
		SerializedProperty lvls = serializedObject.FindProperty("numOfLevels");
		SerializedProperty wave = serializedObject.FindProperty("waves");
		SerializedProperty minX = serializedObject.FindProperty("minSpawnX");
		SerializedProperty maxX = serializedObject.FindProperty("maxSpawnX");
		EditorGUI.BeginChangeCheck();
		EditorGUILayout.PropertyField(lvls, true);
		EditorGUILayout.PropertyField(minX, true);
		EditorGUILayout.PropertyField(maxX, true);
		levelIndex = EditorGUILayout.Popup(levelIndex, levelNames);
		GUILayout.Space(5); 
		GUILayout.Label("Quickly add some enemies", EditorStyles.boldLabel);

		enemyToSpawn = (EnemyBehavior)EditorGUILayout.ObjectField("Enemy", enemyToSpawn, typeof(EnemyBehavior), false);

		numOfEnemies = EditorGUILayout.IntField("Number of enemies", numOfEnemies);

		timeBetween = EditorGUILayout.FloatField("Time between each enemy", timeBetween);
		if(GUILayout.Button("Add enemies!"))
		{
			AddEnemies();
		}

		GUILayout.Space(5); 
		GUILayout.Label("Clear all", EditorStyles.boldLabel);
		if(GUILayout.Button("Clear!"))
		{
			ClearAll();
		}
		GUILayout.Space(5); 
		EditorGUILayout.PropertyField(wave.GetArrayElementAtIndex(levelIndex), new GUIContent("Data"), true);
	
		if(EditorGUI.EndChangeCheck())
		{
			serializedObject.ApplyModifiedProperties();
			UpdateLevelNames();
		}
	
	}

	void UpdateLevelNames()
	{

		levelNames = new string[enemySpawner.numOfLevels];
		for(int i = 1; i < enemySpawner.numOfLevels + 1; i++)
		{
			levelNames[i - 1] = "Level " + i;
		}
		Level[] tempLevels = enemySpawner.waves;
		enemySpawner.waves = new Level[enemySpawner.numOfLevels];
		for(int i = 0; i < tempLevels.Length; i++)
		{
			if(tempLevels[i].enemies != null)
			{
				enemySpawner.waves[i] = tempLevels[i];
			} else
			{
				enemySpawner.waves[i] = new Level();
			}
		}
	}


	void AlignArraySize()
	{
		if(enemySpawner.waves.Length == 0)
		{
			enemySpawner.waves = new Level[1];
		}

		for(int y = 0; y < enemySpawner.numOfLevels; y++)
		{
			Level currentWave = enemySpawner.waves[y];
			int difference = currentWave.enemies.Count - currentWave.spawnTime.Count;
			if(difference > 0)
			{
				for(int i = 0; i < difference; i++)
				{
					currentWave.spawnTime.Add(0);
				}
			} else if(difference < 0)
			{
				for(int i = 0; i < -difference; i++)
				{
					currentWave.enemies.Add(null);
				}
			}
		}
	}

	void AddEnemies()
	{
		if(enemyToSpawn == null)
		{
			return;
		}
		int enemiesLeft = numOfEnemies;
		Level currentWave = enemySpawner.waves[levelIndex];

	
		if(currentWave.enemies.Count == 0)
		{
			currentWave.enemies.Add(null);
		}

		if(currentWave.spawnTime.Count == 0)
		{
			currentWave.spawnTime.Add(0);
		}

		for(int i = 0; i < currentWave.enemies.Count; i++)
		{
			if(currentWave.enemies.Count < enemiesLeft + i + 1)
			{
				currentWave.enemies.Add(null);
			}
			if(currentWave.spawnTime.Count < enemiesLeft + i + 1)
			{
				float tempTime = currentWave.spawnTime[i];
				currentWave.spawnTime.Add(tempTime);

			}

			if(enemiesLeft > 0 && currentWave.enemies[i] == null)
			{
				enemySpawner.waves[levelIndex].enemies[i] = enemyToSpawn.gameObject;
				enemiesLeft--;
				if(i == 0)
				{
					enemySpawner.waves[levelIndex].spawnTime[i] = timeBetween;
				} else
				{
					enemySpawner.waves[levelIndex].spawnTime[i] = enemySpawner.waves[levelIndex].spawnTime[i - 1] + timeBetween;
				}
			}
		}
	}

	void ClearAll()
	{
		enemySpawner.waves[levelIndex].enemies.Clear();
		enemySpawner.waves[levelIndex].spawnTime.Clear();
	}
}