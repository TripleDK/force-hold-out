﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attacker : MonoBehaviour //Maybe Defender?
{
	public EnemyBehavior target;
	[Tooltip("How many attacks per second")]
	public float attackSpeed = 0.2f;
	public float lookAtOffset = 2f;
	[HideInInspector] public EnemySpawner spawner;
	[HideInInspector] public float attackTime = 0f;

	public virtual void Update()
	{
		if(target != null)
		{
			transform.LookAt(target.transform.position - Vector3.up * lookAtOffset);
			if(Time.time - attackTime > 1 / attackSpeed)
			{
				attackTime = Time.time;
				Attack();
			}
		} else
		{
			FindTarget();
		}
	}

	public virtual void Attack()
	{
	}

	public virtual void FindTarget()
	{
		if(spawner.allEnemies.Count > 0)
		{
			target = spawner.allEnemies[Random.Range(0, spawner.allEnemies.Count)];
			attackTime = Time.time;
		}
	}
}
