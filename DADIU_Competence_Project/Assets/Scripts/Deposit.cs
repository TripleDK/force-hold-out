﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class Deposit : MonoBehaviour
{

	public int upgradeLevel = 1;
	public Explodable depositedObject;
	[Tooltip("The cooldown for spawning a new item for each upgrade level")] public float[] spawnCooldown;
	float spawnTime;
	GameObject currentObject;

	void OnEnable()
	{
		SpawnBomb();
	}

	void SpawnBomb()
	{
		currentObject = Instantiate(depositedObject.gameObject, transform.position, transform.rotation);
	}

	void OnTriggerExit(Collider col)
	{
		if(col.gameObject == currentObject)
		{
			StartCoroutine(WaitForNewBomb());	
		}
	}

	IEnumerator WaitForNewBomb()
	{
		yield return new WaitForSeconds(spawnCooldown[upgradeLevel]);
		SpawnBomb();
	}

	void OnDisable()
	{
		Destroy(currentObject);
	}
}
