﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViveGameInput : MonoBehaviour
{

	[SerializeField] JediPowers powersLeft;
	[SerializeField] JediPowers powersRight;
	
	// Update is called once per frame
	void Update()
	{
		if(Input.GetKeyDown("joystick button 14"))
		{
			powersLeft.Grab();
		}
		if(Input.GetKeyDown("joystick button 15"))
		{
			powersRight.Grab();
		}
		if(Input.GetKey("joystick button 14"))
		{
			powersLeft.Drag();
		}
		if(Input.GetKey("joystick button 15"))
		{
			powersRight.Drag();
		}
		if(Input.GetKeyUp("joystick button 14"))
		{
			powersLeft.Release();
		}
		if(Input.GetKeyUp("joystick button 15"))
		{
			powersRight.Release();
		}
		if(Input.GetKeyDown("joystick button 8"))
		{
			powersLeft.Push();
		}
		if(Input.GetKeyDown("joystick button 9"))
		{
			powersRight.Push();
		}

		float leftPull = -Input.GetAxis("LeftTrackPad");
		if(leftPull != 0f)
		{
			powersLeft.Pull(leftPull);
		}		
		float rightPull = -Input.GetAxis("RightTrackPad");
		if(rightPull != 0f)
		{
			powersRight.Pull(rightPull);
		}
	}
}
