﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class MouseCameraMovement : MonoBehaviour
{
	[SerializeField] JediPowers editorHand;
	[SerializeField] float rotationSpeed = 1f;
	Camera cam;
	bool gameFocus;

	// Use this for initialization
	void Start()
	{
		cam = Camera.main;
	}
	
	// Update is called once per frame
	void Update()
	{
		if(gameFocus)
		{
			if(Input.GetMouseButtonDown(0))
			{
				editorHand.Grab();
			}
			if(Input.GetMouseButton(0))
			{

				editorHand.Drag();

			}
			if(Input.GetMouseButtonUp(0))
			{
				editorHand.Release();
			}

			float mouseScroll = Input.mouseScrollDelta.y;
			if(mouseScroll != 0)
			{
				editorHand.Pull(mouseScroll);
			}

			if(Input.GetMouseButtonDown(1))
			{
				editorHand.Push();
			}

			//Camera rotation
			Vector2 mousePos = Input.mousePosition;
			transform.Rotate(new Vector3(-mousePos.y + cam.pixelHeight / 2, mousePos.x - cam.pixelWidth / 2, 0) * rotationSpeed * Time.deltaTime);
			transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, 0);
		}
	}

	void OnApplicationFocus(bool hasFocus)
	{
		gameFocus = hasFocus;
		if(gameFocus == false)
		{
			editorHand.Release();
		}
	}
}
