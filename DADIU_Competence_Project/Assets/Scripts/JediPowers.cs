﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Camera))]
public class JediPowers : MonoBehaviour
{
	[HideInInspector] public Camera cam;
	public List<JediTarget> possibleTargets;
	[SerializeField] List<JediTarget> prevFrameTargets;
	[SerializeField] Image selectionGizmo;
	[SerializeField] Color normalColor;
	[SerializeField] Color selectColor;
	[SerializeField] bool freeFormDragging = true;
	[SerializeField] float forceRange = 100f;
	[SerializeField] float forcePower = 50f;
	[SerializeField] float pullPushPower = 50f;
	[SerializeField] float forcePushPower = 5f;
	[SerializeField] float forcePushCooldown = 10f;
	[SerializeField] float dragDistance = 1f;
	[SerializeField, Tooltip("The force opposite of the current velocity - should be generally lower than forcePower, and bigger the smaller the distance")]
	AnimationCurve dragOverDistance;
	[SerializeField] Camera mainCam;
	RectTransform gizmoCanvas;
	JediTarget closestTarget = null;
	JediTarget currentTarget = null;
	float targetInitDistance, targetInitZ;
	float forcePushTime;
	Vector3 initHandRotation;
	Vector2 uiOffset;


	// Use this for initialization
	void Start()
	{
		possibleTargets = new List<JediTarget>();
		selectionGizmo.enabled = false;
		selectionGizmo.color = normalColor;
		selectionGizmo.transform.localScale = Vector3.one;
		cam = GetComponent<Camera>();
		gizmoCanvas = selectionGizmo.transform.parent.GetComponent<RectTransform>();
		forcePushTime = -forcePushCooldown;
		uiOffset = new Vector2(gizmoCanvas.sizeDelta.x / 2f, gizmoCanvas.sizeDelta.y / 2f);
	}


	void FixedUpdate()
	{
		if(currentTarget != null)
		{
			TargetAddForce();
			TargetAddDragForce();
		}
	}

	void LateUpdate()
	{
		FindClosestTarget(); //This is in late update to ensure all JediTargets have first been added!
	}


	public void Grab()
	{
		if(closestTarget != null)
		{
			currentTarget = closestTarget;
			currentTarget.Grabbed();
			targetInitDistance = (currentTarget.transform.position - transform.position).magnitude;
			targetInitZ = currentTarget.transform.position.z;
			initHandRotation = transform.eulerAngles;
			currentTarget.forceTargetRotation = currentTarget.transform.eulerAngles;

			selectionGizmo.color = selectColor;
			selectionGizmo.transform.localScale = Vector3.one * 0.7f;
		}
	}

	public void Drag()
	{
		if(currentTarget != null)
		{
			if(freeFormDragging)
			{
				currentTarget.forceTargetPosition = transform.position + transform.forward * targetInitDistance;
			} else
			{
				currentTarget.forceTargetPosition = transform.position + transform.forward * targetInitDistance;
				currentTarget.forceTargetPosition.z = targetInitZ;
			}
			currentTarget.forceTargetRotation = currentTarget.initGrabbedRotation + transform.eulerAngles - initHandRotation;
			if((currentTarget.transform.position - transform.position).magnitude > forceRange)
			{
				Release();
			}
		}
	}

	public void Release()
	{
		if(currentTarget != null)
		{
			currentTarget.Release();
			currentTarget = null;
			selectionGizmo.color = normalColor;
			selectionGizmo.transform.localScale = Vector3.one;
		}
	}

	void TargetAddForce()
	{
		
		float distance = (currentTarget.forceTargetPosition - currentTarget.transform.position).magnitude;
		currentTarget.rigid.AddForce((currentTarget.forceTargetPosition - currentTarget.transform.position).normalized * forcePower * Castle.forcePowerBonus * currentTarget.forceOverDistance.Evaluate(distance), ForceMode.Force);
	
	}

	void TargetAddDragForce()
	{
		float offset = (currentTarget.transform.position - currentTarget.forceTargetPosition).magnitude;
		if(offset < dragDistance)
		{
			currentTarget.rigid.AddForce(-currentTarget.rigid.velocity * Castle.forcePowerBonus * dragOverDistance.Evaluate(offset), ForceMode.Force);
		}
	}

	public void Pull(float power)
	{
		targetInitDistance += power * pullPushPower * Time.deltaTime;
		targetInitZ += power * pullPushPower * Time.deltaTime;
	}

	public void Push()
	{
		if(Time.time - forcePushTime > forcePushCooldown)
		{
			forcePushTime = Time.time;
		
			if(closestTarget == null)
			{
				return;
			}
			if(currentTarget == null)
			{
				currentTarget = closestTarget;
			}
				
			Vector3 dir = currentTarget.transform.position - transform.position; 
			float angle = Mathf.Deg2Rad * Vector3.Angle(transform.forward, dir);
			currentTarget.rigid.AddForce(dir * (Mathf.Cos(angle)) * forcePushPower, ForceMode.Impulse);

			Release();

		}
	}

	void FindClosestTarget()
	{
		float closestDistance = 9999;
		prevFrameTargets.Clear();
		foreach(JediTarget target in possibleTargets)
		{
			prevFrameTargets.Add(target);
		}
		if(currentTarget == null)
		{
			if(possibleTargets.Count > 0)
			{
				for(int i = 0; i < possibleTargets.Count; i++)
				{
					Vector2 viewPos = cam.WorldToViewportPoint(possibleTargets[i].transform.position);
					float distance = new Vector2(viewPos.x - .5f, viewPos.y - .5f).magnitude;
					if(distance < closestDistance)
					{
						closestTarget = possibleTargets[i];
						closestDistance = distance;
					}
				}
				if((closestTarget.transform.position - transform.position).magnitude < forceRange)
				{
					EnableGizmo();
				} else
				{
					DisableGizmo();
				}
			} else
			{
				DisableGizmo();
			}
		} else
		{
			EnableGizmo();
		}
		possibleTargets.Clear();
	}

	void EnableGizmo()
	{
		selectionGizmo.enabled = true;
		Vector2 viewPortPosition = mainCam.WorldToViewportPoint(closestTarget.transform.position);
		Vector2 proportionalPosition = new Vector2(viewPortPosition.x * gizmoCanvas.sizeDelta.x, viewPortPosition.y * gizmoCanvas.sizeDelta.y);
		selectionGizmo.rectTransform.localPosition = proportionalPosition - uiOffset;
		selectionGizmo.transform.localEulerAngles = new Vector3(0, 0, transform.localRotation.z);
	
	}

	void DisableGizmo()
	{
		selectionGizmo.enabled = false;
		closestTarget = null;
	}
}


