﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameStateManager : MonoBehaviour
{
	public static int money = 0;

	[SerializeField] Canvas mainMenuCanvas;
	[SerializeField] Canvas upgradeMenuCanvas;
	[SerializeField] Canvas gameUICanvas;
	[SerializeField] EnemySpawner spawner;
	[SerializeField] Castle castle;
	[SerializeField] Shop shop;
	Button loadButton;
	Button saveButton;
	Text levelDisplay;

	// Use this for initialization
	void Awake()
	{
		mainMenuCanvas.gameObject.SetActive(true);
		upgradeMenuCanvas.gameObject.SetActive(false);
		gameUICanvas.gameObject.SetActive(false);
		loadButton = mainMenuCanvas.transform.Find("LoadGame").GetComponent<Button>();
		saveButton = upgradeMenuCanvas.transform.Find("SaveGame").GetComponent<Button>();
		levelDisplay = upgradeMenuCanvas.transform.Find("NextLevel").GetComponent<Text>();

	}

	void OnEnable()
	{
		if(PlayerPrefs.GetInt("Progress") == 0)
		{
			loadButton.interactable = false;
		} else
		{
			loadButton.interactable = true;
		}
	}

	public void StartNewGame()
	{
		mainMenuCanvas.gameObject.SetActive(false);
		EnemySpawner.currentLevel = 0;
		castle.health = 100f;
		money = 0;
		PlayerPrefs.SetInt("Progress", EnemySpawner.currentLevel);
		PlayerPrefs.SetFloat("Health", castle.health);
		PlayerPrefs.SetInt("Money", 0);
		PlayerPrefs.SetInt("BombDeposit", 0);
		StartGame();
	}

	public void StartGame()
	{
		spawner.SpawnWave(EnemySpawner.currentLevel);
		upgradeMenuCanvas.gameObject.SetActive(false);
		gameUICanvas.gameObject.SetActive(true);
		castle.UpdateUI();
		saveButton.interactable = true;
	}

	public void LoadGame()
	{
		mainMenuCanvas.gameObject.SetActive(false);
		EnemySpawner.currentLevel = PlayerPrefs.GetInt("Progress");
		castle.health = PlayerPrefs.GetFloat("Health");
		money = PlayerPrefs.GetInt("Money");
		foreach(Shop.UpgradeableItem upgrade in shop.shopUpgrades)
		{
			upgrade.upgradeLevel = PlayerPrefs.GetInt(upgrade.name, 0);
		}
		castle.RefreshAllCastleUpgrades();
		upgradeMenuCanvas.gameObject.SetActive(true);
		saveButton.interactable = false;
	}

	public void SaveGame()
	{
		PlayerPrefs.SetInt("Progress", EnemySpawner.currentLevel);
		PlayerPrefs.SetFloat("Health", castle.health);
		PlayerPrefs.SetInt("Money", money);
		foreach(Shop.UpgradeableItem upgrade in shop.shopUpgrades)
		{
			PlayerPrefs.SetInt(upgrade.name, upgrade.upgradeLevel);
		}
		saveButton.interactable = false;
	}

	public void WinGame()
	{
		gameUICanvas.gameObject.SetActive(false);
		upgradeMenuCanvas.gameObject.SetActive(true);
		levelDisplay.text = "Next level: " + (EnemySpawner.currentLevel + 1).ToString("D");
	}

	public void LoseGame()
	{
		mainMenuCanvas.gameObject.SetActive(true);
	}
}
