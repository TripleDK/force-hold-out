﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UprootableTree : EnemyBehavior
{
	[SerializeField] float powerToUproot = 100f;
	bool applicationClosing = false;

	public override void OnEnable()
	{
		StopCoroutine("LookForPull");
		rigid.constraints = RigidbodyConstraints.FreezeAll;
	}

	public override void FixedUpdate()
	{
		//Do nothing!!
	}

	public override void Die(Vector3 impulse)
	{
		EnemySpawner.currentNumOfEnemies++; //To counteract base.Die()
		base.Die(impulse);
	}

	void OnApplicationQuit()
	{
		applicationClosing = true;
	}

	public override void OnDisable()
	{
		rigid.constraints = RigidbodyConstraints.None;
		if(!applicationClosing)
		{
			StartCoroutine("LookForPull");
		}
	}

	IEnumerator LookForPull()
	{
		while(true)
		{
			powerToUproot -= rigid.velocity.magnitude * Time.deltaTime;
			if(powerToUproot <= 0)
			{
				Destroy(this);
				yield break;
			}
			rigid.velocity = Vector3.zero;

			yield return new WaitForFixedUpdate();
		}
	}
}
