﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Bomb : Explodable
{
	[SerializeField] float explosionRadius = 10f;
	[SerializeField] float explosionPower = 2f;
	[SerializeField] float damageMultiplier = 3f;
	[SerializeField] GameObject explosionParticles;
	Collider[] colliders;


	void OnDrawGizmosSelected()
	{
		Gizmos.color = new Color(0, 1, 0, .3f);
		Gizmos.DrawSphere(transform.position, explosionRadius);
	}

	public override void BlowUp()
	{
		colliders = Physics.OverlapSphere(transform.position, explosionRadius);
		foreach(Collider otherCol in colliders)
		{
			Rigidbody rb = otherCol.GetComponent<Rigidbody>();

			if(rb != null)
			{
				rb.AddExplosionForce(explosionPower, transform.position, explosionRadius, 0, ForceMode.Impulse);
			} else
			{
				continue;
			}

			EnemyBehavior enemy = otherCol.GetComponent<EnemyBehavior>();

			if(enemy != null)
			{
				enemy.health -= damageMultiplier * explosionPower / ((otherCol.transform.position - transform.position).magnitude / 3 + 0.1f) / enemy.armor;
//				Debug.Log(enemy.gameObject.name + " lost " + (damageMultiplier * explosionPower / ((otherCol.transform.position - transform.position).magnitude + 0.1f)) + " health!");
				enemy.state = EnemyBehavior.EnemyState.FALLING;
			}
		}

		Destroy(Instantiate(explosionParticles, transform.position, explosionParticles.transform.rotation), 3);
		Destroy(gameObject);
	}
}
