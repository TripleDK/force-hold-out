﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explodable : MonoBehaviour
{
	public float impulseThreshold = 5f;

	public virtual void OnCollisionEnter(Collision col)
	{
		if(col.impulse.magnitude > impulseThreshold)
		{
			BlowUp();
		}
	}

	public virtual void BlowUp()
	{
	}
}
