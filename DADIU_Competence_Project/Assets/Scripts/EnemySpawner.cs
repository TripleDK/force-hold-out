﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Level
{
	public bool preDeterminedSpawn;
	public List<GameObject> enemies;
	public List<float> spawnTime;
	public AnimationCurve randomTimeRange;
}

public class EnemySpawner : MonoBehaviour
{
	public static int currentLevel = 0;
	public static int currentNumOfEnemies = 0;
	public Castle castle;
	public int numOfLevels;
	public Level[] waves;
	[HideInInspector] public List<EnemyBehavior> allEnemies = new List<EnemyBehavior>();
	[SerializeField] Transform spawnPoint;
	[SerializeField, Range(-500, 0)] float minSpawnX;
	[SerializeField, Range(0, 500)] float maxSpawnX;
	[SerializeField] GameStateManager gameState;

	float waveStartTime;
	float lastSpawnTime;
	float randomDelay;
	Level currentWave;

	void Start()
	{
		spawnPoint = transform.GetChild(0);
		gameState = FindObjectOfType<GameStateManager>();
	}

	public void SpawnWave(int wave)
	{
		waveStartTime = Time.time;
		currentNumOfEnemies = 0;
		allEnemies.Clear();
		lastSpawnTime = 0f;
		currentWave = waves[currentLevel];
		randomDelay = 0f;	
		for(int i = 0; i < currentWave.enemies.Count; i++)
		{
			if(currentWave.enemies[i] != null)
			{
				currentNumOfEnemies++;
				if(currentWave.spawnTime[i] > lastSpawnTime)
				{
					lastSpawnTime = currentWave.spawnTime[i];
				}
			}
		}
		if(currentWave.preDeterminedSpawn)
		{
			StartCoroutine(SpawningInOrder(currentWave));
		} else
		{
			StartCoroutine(SpawningRandomly(currentWave));
		}
	}

	IEnumerator SpawningInOrder(Level wave)
	{
		for(int i = 0; i < currentWave.enemies.Count; i++)
		{
			while(Time.time - waveStartTime < currentWave.spawnTime[i] + randomDelay)
			{
				yield return null;
			}
			if(currentWave.enemies[i] == null)
			{
				break;
			}
			SpawnEnemy(currentWave.enemies[i]);
			CalculateRandomDelay(i);
		}
	}

	IEnumerator SpawningRandomly(Level wave)
	{
		int maxNumOfEnemies = currentNumOfEnemies;
		for(int i = 0; i < maxNumOfEnemies; i++)
		{
			while(Time.time - waveStartTime < currentWave.spawnTime[i] + randomDelay)
			{
				yield return null;
			}
			int enemyIndex = Random.Range(0, currentWave.enemies.Count);
			int whileCount = 0;
			while(currentWave.enemies[enemyIndex] == null)
			{
				enemyIndex++;
				if(enemyIndex > currentWave.enemies.Count - 1)
				{
					enemyIndex = 0;
				}
				whileCount++;
				if(whileCount > currentWave.enemies.Count)
				{
					Debug.LogError("Cannot find enemy!");
					break;
				}
			}
			SpawnEnemy(currentWave.enemies[enemyIndex]);
			currentWave.enemies.RemoveAt(enemyIndex);
			CalculateRandomDelay(i);
		}
	}

	void SpawnEnemy(GameObject enemy)
	{
		EnemyBehavior tempEnemy = Instantiate(enemy,
			                          spawnPoint.position + Vector3.right * Random.Range(minSpawnX, maxSpawnX) - Vector3.up * enemy.GetComponent<Collider>().bounds.extents.y,
			                          enemy.transform.rotation).GetComponent<EnemyBehavior>();
		tempEnemy.castle = castle;
		tempEnemy.spawner = this;
		allEnemies.Add(tempEnemy);
	}

	void CalculateRandomDelay(int index)
	{
		if(currentWave.spawnTime[index] == 0)
		{
			randomDelay = 0;
		} else
		{
			randomDelay = currentWave.randomTimeRange.Evaluate(
				(Time.time - waveStartTime) / lastSpawnTime);	
		}
		randomDelay = Random.Range(-randomDelay, randomDelay);
	}

	public void LoseGame()
	{
		foreach(EnemyBehavior enemy in allEnemies)
		{
			if(enemy != null)
			{
				Destroy(enemy.gameObject, 3);
				Destroy(enemy);
			}
		}
		gameState.LoseGame();
	}

	public void WinGame()
	{
		currentLevel++;
		gameState.WinGame();
	}


}
