﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class JediTarget : MonoBehaviour
{
	public bool forced;
	public AnimationCurve forceOverDistance;
	[HideInInspector] public Vector3 forceTargetPosition, forceTargetRotation, initGrabbedRotation;
	[HideInInspector] public Rigidbody rigid;
	JediPowers[] jedis;
	EnemyBehavior behavior;



	// Use this for initialization
	void Awake()
	{
		jedis = (JediPowers[])FindObjectsOfType(typeof(JediPowers));
		rigid = GetComponent<Rigidbody>();
		forced = false;

		behavior = GetComponent<EnemyBehavior>();
		if(forceOverDistance.keys.Length == 0)
		{
			Debug.LogError("No force over distance applied at " + gameObject.name);
		}
	
	}
	
	// Update is called once per frame
	void Update()
	{
		foreach(JediPowers jedi in jedis)
		{
			Camera cam = jedi.cam;
			Vector3 viewPos = cam.WorldToViewportPoint(transform.position);
			if(viewPos.x > 0 && viewPos.x < 1 && viewPos.y > 0 && viewPos.y < 1 && viewPos.z > 0)
			{
				jedi.possibleTargets.Add(this);
			}
		}
	}


	public void Grabbed()
	{
		forced = true;
		initGrabbedRotation = transform.eulerAngles;
		rigid.useGravity = true;
		if(behavior != null)
		{
			behavior.enabled = false;
		}

	}

	public void Release()
	{
		forced = false;	
		if(behavior != null)
		{
			behavior.enabled = true;
		}
	}

	void OnDestroy()
	{
		if(jedis == null)
		{
			Debug.Log("Waddafuck " + gameObject.name);
		}
		foreach(JediPowers jedi in jedis)
		{
			jedi.possibleTargets.Remove(this);
		}
	}
}
