﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
public class Turret : Attacker
{
	public float damage = 50f;

	[SerializeField] GameObject shotParticles;
	[SerializeField] GameObject muzzleFlash;
	[SerializeField] Transform endOfBarrelPos;

	public override void Attack()
	{
		attackTime = Time.time;
		Destroy(Instantiate(muzzleFlash, endOfBarrelPos.position, endOfBarrelPos.rotation), 5);
		Destroy(Instantiate(shotParticles, target.transform.position, target.transform.rotation), 5);
		target.health -= damage / target.armor;
		target.rigid.AddForce((target.transform.position - transform.position).normalized * damage, ForceMode.Impulse);
		if(target.health <= 0)
		{
			target.Die(damage * (target.transform.position - transform.position).normalized);
			if(spawner.allEnemies.Count > 0)
			{
				target = spawner.allEnemies[Random.Range(0, spawner.allEnemies.Count)];
			} else
			{
				target = null;
			}
		}
	}
}
