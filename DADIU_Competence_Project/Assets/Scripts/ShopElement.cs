﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopElement : MonoBehaviour
{
	public ShopButton shopButton;
	public Text name, upgradeLevel, price;
	public RectTransform contentRectTransform;

}
