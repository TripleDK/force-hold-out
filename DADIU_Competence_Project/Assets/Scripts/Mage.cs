﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mage : Attacker
{
	[SerializeField] float attackHeight = 15f;
	[SerializeField] float forcePower = 5f;
	Collider groundCol;
	JediTarget jediTarget;
	bool smash = false;
	Collider targetCol;

	void Awake()
	{
		groundCol = GameObject.FindGameObjectWithTag("Ground").GetComponent<Collider>();
	}

	public override void Attack()
	{
		jediTarget = target.gameObject.GetComponent<JediTarget>();
		if(jediTarget != null)
		{
			jediTarget.Grabbed();
			StartCoroutine(Smash());
		} else
		{
			target = null;
		}
	}

	IEnumerator Smash()
	{
		while(target != null)
		{
			yield return null;
			if(jediTarget == null)
			{
				target = null;
				break;
			}
			if(smash == false)
			{
				jediTarget.rigid.AddForce(Vector3.up * forcePower * Castle.forcePowerBonus, ForceMode.Force);
			} else
			{
				jediTarget.rigid.AddForce(-Vector3.up * forcePower * Castle.forcePowerBonus, ForceMode.Force);

				if(targetCol.bounds.Intersects(groundCol.bounds))
				{
					jediTarget.Release();
					smash = false;
					attackTime = Time.time;
					target = null;
				}
			}

			if(jediTarget.transform.position.y > attackHeight)
			{
				smash = true;
			}
			attackTime = Time.time;

		}
	}

	public override void FindTarget()
	{
		base.FindTarget();
		if(target != null)
		{
			targetCol = target.GetComponent<Collider>();
		}
	}
}
