﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Castle : MonoBehaviour
{
	public static float forcePowerBonus = 1;

	public float maxHealth = 100f;
	public float health = 100f;
	[HideInInspector] public Bounds bounds;

	[SerializeField] Text numOfEnemies;
	[SerializeField] Text currentLevel;
	[SerializeField] Text points;
	[SerializeField] Slider healthSlider;
	[SerializeField] EnemySpawner spawner;
	[SerializeField] Shop shop;
	[SerializeField] BombDeposit bombDepositPos;
	[SerializeField] OilDeposit oilDepositPos;
	[SerializeField] List<Transform> turretPos = new List<Transform>();
	[SerializeField] Turret turret;
	[SerializeField] List<Transform> magePos = new List<Transform>();
	[SerializeField] Mage mage;
	[SerializeField] Transform fountainOfPower;
	List<Turret> turrets = new List<Turret>();
	List<Mage> mages = new List<Mage>();

	// Use this for initialization
	void Start()
	{
		healthSlider.maxValue = health;
		healthSlider.minValue = 0;
		healthSlider.value = health;
		bounds = GetComponent<BoxCollider>().bounds;
		bombDepositPos.gameObject.SetActive(false);
		oilDepositPos.gameObject.SetActive(false);
		UpdateUI();
	}

	public void UpdateUI()
	{
		numOfEnemies.text = "Enemies left: " + EnemySpawner.currentNumOfEnemies;
		currentLevel.text = "Current level: " + (EnemySpawner.currentLevel + 1).ToString("D");
		healthSlider.value = health;
		points.text = "Points: " + GameStateManager.money.ToString("D");
	}

	public void Attack(float damage)
	{
		health -= damage;
		healthSlider.value = health;
		if(health <= 0)
		{
			Die();
		}
	}

	public void Die()
	{
		spawner.LoseGame();
		numOfEnemies.text = "You lost!";
	}

	public void RefreshCastleUpgrade(string upgradeName)
	{
		
		Shop.UpgradeableItem upgrade = shop.shopUpgrades.Where(o => o.name == upgradeName).SingleOrDefault();
		switch(upgradeName)
		{
		case "Bomb Deposit": 
			Debug.Log("Bomb deposit upgrade level: " + upgrade.upgradeLevel);
			if(upgrade.upgradeLevel >= 1)
			{
				bombDepositPos.gameObject.SetActive(true);
				bombDepositPos.upgradeLevel = upgrade.upgradeLevel;
			}
			break;
		case "Mages":
			Debug.Log("Mage upgrade level: " + upgrade.upgradeLevel);
			if(mages.Count < upgrade.upgradeLevel)
			{
				if(magePos.Count < upgrade.upgradeLevel)
				{
					Debug.LogError("Not enough mage positions!");
					break;
				}
				mages.Add(Instantiate(mage, magePos[mages.Count].position, magePos[mages.Count].rotation));
				mages[mages.Count - 1].spawner = spawner;
			}
			break;	
		case "Oil Barrel": 
			Debug.Log("Oil barrel upgrade level: " + upgrade.upgradeLevel);
			if(upgrade.upgradeLevel >= 1)
			{
				oilDepositPos.gameObject.SetActive(true);
				oilDepositPos.upgradeLevel = upgrade.upgradeLevel;
			}
			break;
		case "Force Power": 
			Debug.Log("Force power upgrade level: " + upgrade.upgradeLevel);
			forcePowerBonus = 1 + upgrade.upgradeLevel * 0.3f;
			fountainOfPower.localScale = Vector3.one * upgrade.upgradeLevel;
			fountainOfPower.transform.localPosition = Vector3.up * upgrade.upgradeLevel;
			break;
		case "Turret Attack Speed":
			Debug.Log("Turret Attack Speed upgrade level: " + upgrade.upgradeLevel);
			foreach(Turret turret in turrets)
			{
				turret.attackSpeed = 0.2f * upgrade.upgradeLevel;
			}
			break;
		case "Turrets":
			Debug.Log("Turret number upgrade level: " + upgrade.upgradeLevel);
			if(turrets.Count < upgrade.upgradeLevel)
			{
				if(turretPos.Count < upgrade.upgradeLevel)
				{
					Debug.LogError("Not enough turret positions!");
					break;
				}
				turrets.Add(Instantiate(turret, turretPos[turrets.Count].position, turretPos[turrets.Count].rotation));
				turrets[turrets.Count - 1].spawner = spawner;
			}
			break;
		case "Max Health":
			maxHealth = 100 + 25 * upgrade.upgradeLevel;
			shop.castleHealth.text = "Castle Health: \n" + health + "/" + maxHealth;
			break;
		default:
			Debug.Log(upgrade.name + " is not a registered upgrade!");
			Debug.Assert(false);
			break;

		}
	}

	public void RefreshAllCastleUpgrades()
	{
		foreach(Shop.UpgradeableItem upgrade in shop.shopUpgrades)
		{
			RefreshCastleUpgrade(upgrade.name);
		}
	}

	public void ConsumeShopItem(string itemName)
	{
		Shop.PurchaseableItem item = shop.shopConsumables.Where(o => o.name == itemName).SingleOrDefault();
		switch(itemName)
		{
		case "Health 10":
			AddHealth(10);
			break;
		case "Health 50":
			AddHealth(50);
			break;
		default:
			Debug.Log(item.name + " is not a registered item!");
			Debug.Assert(false);
			break;
		}
	}

	void AddHealth(int health)
	{
		this.health += health;
		if(this.health > maxHealth)
		{
			this.health = maxHealth;
		}
		shop.castleHealth.text = "Castle Health: \n" + this.health + "/" + maxHealth;
	}
}
