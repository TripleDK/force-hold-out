﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedEnemy : EnemyBehavior
{

	[SerializeField] float range = 100f;
	[SerializeField] float attackAngle = 45f;
	[SerializeField] EnemyProjectile projectile;

	public override void Move()
	{
		base.Move();
		if((targetPoint - transform.position).magnitude < range)
		{
			state = EnemyState.ATTACKING;
			rigid.velocity = Vector3.zero;
		}
	}


	public override	void Attack()
	{
		if(Time.time - attackTime > 1 / attackSpeed)
		{
			attackTime = Time.time;
			EnemyProjectile tempProjectile = Instantiate(projectile, transform.position, Quaternion.identity);
			Physics.IgnoreCollision(coll, tempProjectile.GetComponent<Collider>());
			tempProjectile.damage = damage;
			tempProjectile.castle = castle;
			Vector3 shootDir = transform.forward;
			shootDir = Vector3.RotateTowards(shootDir, transform.up, attackAngle * Mathf.Deg2Rad, 0);
			shootDir *= CalculateThrowForce();
			tempProjectile.rigid.velocity = shootDir;
			Destroy(tempProjectile.gameObject, 10);
			//	Debug.Break();
		}
	}


	float CalculateThrowForce()
	{
		//d = v^2 /g  * sin(2*angle)
		//v^2 = d * g / sin(2*angle)

		float distance = (targetPoint - transform.position).magnitude;
		float initVelocity = Mathf.Sqrt(Mathf.Abs(distance * Physics.gravity.y / Mathf.Sin(2 * attackAngle * Mathf.Deg2Rad)));
		return initVelocity;
	}
}
