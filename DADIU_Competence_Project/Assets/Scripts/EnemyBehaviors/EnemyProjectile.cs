﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class EnemyProjectile : MonoBehaviour
{
	public float damage = 5;
	[HideInInspector] public Rigidbody rigid;
	[HideInInspector] public Castle castle;
	[SerializeField] GameObject explosionParticles;

	void Awake()
	{
		rigid = GetComponent<Rigidbody>();
	}


	void OnCollisionEnter(Collision col)
	{
		if(col.gameObject.tag == "Castle")
		{
			castle.Attack(damage);
			Destroy(Instantiate(explosionParticles, transform.position, explosionParticles.transform.rotation), 5);
			Destroy(gameObject);
		}
	}
}
