﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
public class EnemyBehavior : MonoBehaviour
{
	[HideInInspector] public EnemySpawner spawner;
	public EnemyState state;
	public Castle castle;
	public float health = 100f;
	[Range(0.1f, 100f), Tooltip("Any damage taken is divided with this property")]
	public float armor = 1f;
	public float damage = 1f;
	[Tooltip("How many attacks per second?")] 
	public float attackSpeed = 1f;
	[Tooltip("The velocity before the enemy starts regaining movement control")]
	public float slowDownThreshold = 2f;
	public float movementForce = 10f;
	[HideInInspector] public Vector3 targetPoint;
	[HideInInspector] public float attackTime = 0;
	[HideInInspector] public Collider coll;
	[HideInInspector] public Rigidbody rigid;
	[SerializeField] float maxSpeed = 6f;
	[SerializeField] float riseSpeed = 1f;
	[SerializeField] int pointsWorth = 100;
	[SerializeField] GameObject attackParticles;
	[SerializeField] ParticleSystem deathParticles;

	JediTarget jediTarget;
	Renderer rend;

	public enum EnemyState
	{
		FALLING,
		GETTING_UP,
		ATTACKING,
		MOVING,
		DEAD
	}

	// Use this for initialization
	public virtual void Awake()
	{
		rigid = GetComponent<Rigidbody>();
		coll = GetComponent<Collider>();
		jediTarget = GetComponent<JediTarget>(); //Might not be found!!!
		if(castle == null)
		{
			castle = FindObjectOfType<Castle>();
		}
	}


	public virtual void OnEnable()
	{
		state = EnemyState.FALLING;
	}
		

	// Update is called once per frame
	public virtual void FixedUpdate()
	{
		switch(state)
		{
		case EnemyState.FALLING:
			CheckGround();
			break;
		case EnemyState.GETTING_UP:
			GetUp();
			break;
		case EnemyState.MOVING:
			Move();
			break;
		case EnemyState.DEAD:
			break;
		case EnemyState.ATTACKING:
			Attack();
			break;
		default:
			break;
		}
		if(transform.position.y < -100)
		{
			Debug.Log("Someone flew off the map :(");
			Die(Vector3.zero);
		}
	}

	Vector3 FindRandomCastlePoint()
	{
		Vector3 point = new Vector3(Random.Range(castle.bounds.min.x, castle.bounds.max.x),
			                Random.Range(castle.bounds.min.y, castle.bounds.max.y),
			                Random.Range(castle.bounds.min.z, castle.bounds.max.z));
		targetPoint = point;
		return point;
	}

	Vector3 FindClosestCastlePoint()
	{
		Vector3 point = castle.bounds.ClosestPoint(transform.position);
		Debug.Log(point);
		targetPoint = point;
		return point;
	}

	public virtual void CheckGround()
	{
		if(rigid.velocity.magnitude < slowDownThreshold)
		{
			if(Physics.Raycast(transform.position, -Vector3.up, coll.bounds.extents.y + 0.1f))
			{
				state = EnemyState.GETTING_UP;
				rigid.velocity = Vector3.zero;		
			}
		}
	}

	public virtual void GetUp()
	{
		Quaternion q = Quaternion.RotateTowards(transform.rotation, Quaternion.identity, riseSpeed);
	
		rigid.MoveRotation(q);
		if(q.eulerAngles == Vector3.zero)
		{
			state = EnemyState.MOVING;
			transform.eulerAngles = Vector3.zero;
			transform.LookAt(FindRandomCastlePoint());
			rigid.constraints = RigidbodyConstraints.FreezeRotation;
			rigid.useGravity = true;
			rigid.velocity = Vector3.zero;
		}
	}

	public virtual void Move()
	{
		rigid.AddForce(transform.forward * movementForce, ForceMode.Force);
		if(Mathf.Abs(rigid.velocity.magnitude) > maxSpeed)
		{
			rigid.velocity = rigid.velocity.normalized * maxSpeed;
		}
	}

	public virtual void Attack()
	{
		if(Time.time - attackTime > 1 / attackSpeed)
		{
			attackTime = Time.time;
			castle.Attack(damage);	
			Destroy(Instantiate(attackParticles, transform.position, transform.rotation), 5);
		}
	}

	public virtual void Die(Vector3 impulse)
	{
		state = EnemyState.DEAD;

		EnemySpawner.currentNumOfEnemies--;
		spawner.allEnemies.Remove(this);
		if(EnemySpawner.currentNumOfEnemies <= 0)
		{
			spawner.WinGame();
		}

		GameStateManager.money += pointsWorth;
		castle.UpdateUI();
		if(jediTarget != null)
		{
			Destroy(jediTarget);
		}
		ParticleSystem tempBlood = Instantiate(deathParticles, transform.position, Quaternion.Euler(impulse));
		tempBlood.transform.LookAt(transform.position + impulse);
		tempBlood.transform.Translate(impulse.normalized * coll.bounds.extents.y, Space.World);
		ParticleSystem.Burst[] burst = new ParticleSystem.Burst[1];
		burst[0].minCount = (short)(impulse.magnitude / armor / 10);
		tempBlood.emission.SetBursts(burst);
		Destroy(tempBlood, 3);
		Destroy(gameObject, 3);
		Destroy(this);
	}

	public void OnCollisionEnter(Collision col)
	{
		if(col.gameObject.tag == "Enemy" || col.gameObject.tag == "Obstacle")
		{
			EnemyBehavior enemy = col.gameObject.GetComponent<EnemyBehavior>();
			if(enemy != null && col.gameObject.GetComponent<EnemyBehavior>().state == EnemyState.ATTACKING)
			{
				transform.LookAt(FindRandomCastlePoint());
				return;
			}
			state = EnemyState.FALLING;
		}
		if(col.gameObject.tag == "Castle" && this.enabled)
		{
			if(state == EnemyState.MOVING)
			{
				state = EnemyState.ATTACKING;
			}
		} else
		{
			health -= col.impulse.magnitude / armor;
		}
		if(health <= 0)
		{
			Die(col.impulse);
		}
	}


	public virtual void OnDisable()
	{
		rigid.constraints = RigidbodyConstraints.None;
		state = EnemyState.FALLING;
	}

}
