﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Bomb))]
public class SuicideBomber : EnemyBehavior
{

	Bomb bomb;

	// Use this for initialization
	public override void Awake()
	{
		bomb = GetComponent<Bomb>();
		base.Awake();
	}

	public override void Attack()
	{
		castle.Attack(damage);
		Die(Vector3.one * 50);
	}

	public override	void Die(Vector3 impulse)
	{
		bomb.BlowUp();
		base.Die(impulse);
	}
}
