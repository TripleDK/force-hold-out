﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingEnemy : EnemyBehavior
{

	[SerializeField] float idealHeight = 10;
	[SerializeField] float heightRandomness = 2;
	[SerializeField] float ascendForce = 5f;
	[SerializeField] float range = 50;
	[SerializeField] float projectileSpeed = 2f;
	[SerializeField] EnemyProjectile projectile;

	public override void Awake()
	{
		base.Awake();
		idealHeight += Random.Range(-heightRandomness, heightRandomness);
	}

	public override void Move()
	{
		base.Move();
		rigid.AddForce(-Physics.gravity);
		if(transform.position.y < idealHeight)
		{
			rigid.AddForce(Vector3.up * ascendForce, ForceMode.Force);
		} else
		{
			rigid.AddForce(-Vector3.up * ascendForce, ForceMode.Force);
		}
		if((targetPoint - transform.position).magnitude < range && Mathf.Abs(transform.position.y - idealHeight) < heightRandomness)
		{
			state = EnemyState.ATTACKING;
			rigid.velocity = Vector3.zero;
		}
	}

	public override void GetUp()
	{
		base.GetUp();
		rigid.AddForce(-Physics.gravity);
		rigid.AddForce(-rigid.velocity / 2);
	}

	public override void Attack()
	{
		rigid.AddForce(-Physics.gravity);
		if(Time.time - attackTime > 1 / attackSpeed)
		{
			attackTime = Time.time;
			EnemyProjectile tempProjectile = Instantiate(projectile, transform.position, transform.rotation);
			tempProjectile.rigid.useGravity = false;
			tempProjectile.castle = castle;
			tempProjectile.transform.LookAt(targetPoint);
			tempProjectile.damage = damage;
			tempProjectile.rigid.velocity = tempProjectile.transform.forward * projectileSpeed;
			Physics.IgnoreCollision(coll, tempProjectile.GetComponent<Collider>());

		}
	}

	public override void CheckGround()
	{
		if(rigid.velocity.magnitude < slowDownThreshold)
		{
			state = EnemyState.GETTING_UP;
		}
	}
}
